
DROP TABLE IF EXISTS NNotes
GO
DROP TABLE IF EXISTS NUsers
GO


CREATE TABLE NUsers
(
	UserId NVARCHAR(40) PRIMARY KEY NOT NULL,
	UserName NVARCHAR(40) NOT NULL,
	Email NVARCHAR(80) NOT NULL,
	Pwd NVARCHAR(25) NOT NULL,
)
GO

CREATE TABLE NNotes
(
	NoteID INT IDENTITY PRIMARY KEY,
	UserId NVARCHAR(40) FOREIGN KEY REFERENCES NUsers(UserId),
	Title NVARCHAR(50) NOT NULL,
	MarkUp NVARCHAR(MAX) NOT NULL
)
GO

INSERT INTO NUsers
	(UserId, UserName, Email, Pwd)
	VALUES
	('b.patel405', 'Brijesh Patel', 'b.patel405@mybvc.ca', '1234'),
	('h.do123', 'Hai Do', 'h.do123@mybvc.ca', '1234'),
	('s.saini123', 'Sandeep Saini', 's.saini123@mybvc.ca', '1234')
GO

INSERT INTO NNotes
	(UserId, Title, MarkUp)
	VALUES
	('b.patel405', 'WP', 'Web Programming'),
	('h.do123', 'MGMT', 'Project Management')
GO

