# SODV2101_RAD_GP

## group name : Eniac

## Group Members 
    - Brijeshkumar Patel
    - Pablo Winter
    - Sandeep Saini

## Instruction to work with App

- Use following credentials to login and logout  

     1) Email: b.patel405@mybvc.ca
        Password : 1234
        
     2) Email:p.rodriguez673@mybvc.ca
        Password: 1234
        
     3) Email:b.patel9528@gmail.com
        Password: 1234  
        
        
- First button on NoteControl is delete button
     delte works async

- click on Image Icon to Import image to pdf

- click on B, I, U, ColorIcon, Font Icon to change style of font (select font before if you want to modify text which was written already)

- click on Print Icon to export note as word document
    Note:- we remove printing feature because word document is easy to share and print than doing everything by ourselves and limiting perfomance of App.
