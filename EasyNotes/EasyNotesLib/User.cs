﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;

namespace EasyNotesLib
{
    public class User
    {
        public string UserId { get; private set; }
        public string UserName { get; set; }
        public string Password { get; private set; }
        public string Email { get; private set; }
        public List<Note> Notes { get; private set; }
        public bool IsLoggedIn { get; private set; }

        public User(string Email, string pwd)
        {
            //authenticate user
            AuthenticateUser(Email, pwd);
            Notes = new List<Note>();
            //if user authenticated then get all the notes
            if (IsLoggedIn)
            {
                using (var context = new DataContext(Helper.GetConnectionString()))
                {
                    var Notes = context.GetTable<NNote>().Where(note => note.UserId == this.UserId).ToList();
                    foreach (var note in Notes)
                    {
                        this.Notes.Add(new Note(note.Title, note.MarkUp, note.NoteID));
                    }
                }
            }
            else
            {
                throw new Exception("Invalid credentials provided!");
            }
        }

        public User() { }

        private void AuthenticateUser(string email, string pwd)
        {
            using (var context = new DataContext(Helper.GetConnectionString()))
            {
                var users = context.GetTable<NUser>().Where(user => user.Email == email && user.Pwd == pwd).ToList();
                if (users.Count > 0)
                {
                    this.UserId = users[0].UserId;
                    this.UserName = users[0].UserName;
                    this.Email = users[0].Email;
                    this.Password = users[0].Pwd;
                    this.IsLoggedIn = true;
                }
                else
                {
                    this.Email = email;
                    this.Password = pwd;
                    this.IsLoggedIn = false;
                }
            }
        }

        //save note to databases
        public void SaveNote(Note note)
        {
            //if noteid == -1, note is note saved in database
            // -1 id default id
            // if already saved update note
            if (note.NoteID == -1)
            {
                this.Notes.Add(note);
                var noteRecord = new NNote()
                    { Title = note.Title, MarkUp = note.MarkUp, UserId = this.UserId };
                note.NoteID = SaveNewNote(noteRecord);
            }
            else
            {
                this.Notes.Where(anote => anote.NoteID == note.NoteID).ToList()[0] = note;
                UpdateNoteToDB(note);
            }
        }

        //update note to database 
        private void UpdateNoteToDB(Note note)
        {
            using (var contex = new DataContext(Helper.GetConnectionString()))
            {
                var databaseNote = contex.GetTable<NNote>().Where(anote => anote.NoteID == note.NoteID).ToList()[0];
                databaseNote.MarkUp = note.MarkUp;
                databaseNote.Title = note.Title;
                contex.SubmitChanges();
            }
        }

        //save new note to database
        private int SaveNewNote(NNote noteRecord)
        {
            using (var contex = new DataContext(Helper.GetConnectionString()))
            {
                contex.GetTable<NNote>().InsertOnSubmit(noteRecord);
                contex.SubmitChanges();
            }
            return noteRecord.NoteID;
        }

        public void DeleteNote(Note note)
        {
            this.Notes.Remove(note);
            //if note has default id (not save to database) no need to delete from database
            if (note.NoteID == -1)
            {
                return;
            }
            DeleteFromDB(note.NoteID);
        }

        //find note and delete it from database
        private void DeleteFromDB(int noteId)
        {
            using (var contex = new DataContext(Helper.GetConnectionString()))
            {
                var filteredNote  = contex.GetTable<NNote>().Where(note => note.NoteID == noteId).ToList()[0];
                contex.GetTable<NNote>().DeleteOnSubmit(filteredNote);
                contex.SubmitChanges();
            }
        }

        //function to generate username when user register with username
        private string GetUserId(string email)
        {
            return email.Substring(0, email.IndexOf('@'));
        }

        //insert new user to the database when user registers
        public void InsertNewUser(string userName, string email, string password)
        {
            // Generating user ID for user
            var newUserId = GetUserId(email);

            using (NNotesDataContext context = new NNotesDataContext())
            {
                NUser newUser = new NUser
                {
                    UserId = newUserId,
                    UserName = userName,
                    Email = email,
                    Pwd = password
                };

                context.NUsers.InsertOnSubmit(newUser);
                context.SubmitChanges();
                this.IsLoggedIn = true;
            }
        }
    }
}
