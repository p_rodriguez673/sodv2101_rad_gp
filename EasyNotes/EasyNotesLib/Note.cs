﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyNotesLib
{
    public class Note
    {
        public int NoteID { get; set; }
        public string Title { get; set; }
        public string MarkUp { get; set; }

        public Note(string title, string markup = "", int id = -1)
        {
            this.NoteID = id;
            this.MarkUp = markup;
            this.Title = title;
        }

        public override string ToString()
        {
            return NoteID + " " + Title + " " + MarkUp;
        }
    }
}
