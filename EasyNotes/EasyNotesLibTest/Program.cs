﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyNotesLib;

namespace EasyNotesLibTest
{
    class Program
    {
        static void Main(string[] args)
        {
            User newUser = new User("b.patel405@mybvc.ca", "1234");
            Note n = new Note("OOP");
            n.NoteID = 1;
            newUser.SaveNote(n);
            Console.ReadKey();
        }
    }
}
