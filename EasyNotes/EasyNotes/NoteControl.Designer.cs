﻿namespace EasyNotes
{
    partial class NoteControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NoteControl));
            this.PrintPreviewNote = new System.Windows.Forms.PrintPreviewDialog();
            this.PrintNoteDocument = new System.Drawing.Printing.PrintDocument();
            this.NoteTextBox = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Title_Textbox = new System.Windows.Forms.TextBox();
            this.ChangeFontItalic = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeFontUnderline = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeFontColor = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeFontSize = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteNoteOption = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToPdfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NoteEditorTools = new System.Windows.Forms.MenuStrip();
            this.ChangeFontBold = new System.Windows.Forms.ToolStripMenuItem();
            this.addImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.NoteEditorTools.SuspendLayout();
            this.SuspendLayout();
            // 
            // PrintPreviewNote
            // 
            this.PrintPreviewNote.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.PrintPreviewNote.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.PrintPreviewNote.ClientSize = new System.Drawing.Size(400, 300);
            this.PrintPreviewNote.Enabled = true;
            this.PrintPreviewNote.Icon = ((System.Drawing.Icon)(resources.GetObject("PrintPreviewNote.Icon")));
            this.PrintPreviewNote.Name = "PrintPreviewNote";
            this.PrintPreviewNote.Visible = false;
            // 
            // PrintNoteDocument
            // 
            // 
            // NoteTextBox
            // 
            this.NoteTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NoteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NoteTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoteTextBox.Location = new System.Drawing.Point(0, 64);
            this.NoteTextBox.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.NoteTextBox.Name = "NoteTextBox";
            this.NoteTextBox.Size = new System.Drawing.Size(580, 257);
            this.NoteTextBox.TabIndex = 3;
            this.NoteTextBox.Text = "";
            this.NoteTextBox.TextChanged += new System.EventHandler(this.NoteTextBox_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.Title_Textbox);
            this.panel1.Location = new System.Drawing.Point(0, 29);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(578, 42);
            this.panel1.TabIndex = 4;
            // 
            // Title_Textbox
            // 
            this.Title_Textbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Title_Textbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Title_Textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title_Textbox.Location = new System.Drawing.Point(0, 2);
            this.Title_Textbox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Title_Textbox.Name = "Title_Textbox";
            this.Title_Textbox.Size = new System.Drawing.Size(577, 35);
            this.Title_Textbox.TabIndex = 5;
            this.Title_Textbox.Text = "untitled";
            this.Title_Textbox.TextChanged += new System.EventHandler(this.Title_TextChanged);
            // 
            // ChangeFontItalic
            // 
            this.ChangeFontItalic.BackgroundImage = global::EasyNotes.Properties.Resources.italics_font_style_variant;
            this.ChangeFontItalic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ChangeFontItalic.Margin = new System.Windows.Forms.Padding(5, 6, 7, 0);
            this.ChangeFontItalic.Name = "ChangeFontItalic";
            this.ChangeFontItalic.Size = new System.Drawing.Size(22, 21);
            this.ChangeFontItalic.Text = " ";
            this.ChangeFontItalic.Click += new System.EventHandler(this.ChangeFontItalic_Click);
            // 
            // ChangeFontUnderline
            // 
            this.ChangeFontUnderline.BackgroundImage = global::EasyNotes.Properties.Resources.underline_text_option;
            this.ChangeFontUnderline.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ChangeFontUnderline.Margin = new System.Windows.Forms.Padding(5, 5, 7, 0);
            this.ChangeFontUnderline.Name = "ChangeFontUnderline";
            this.ChangeFontUnderline.Size = new System.Drawing.Size(25, 22);
            this.ChangeFontUnderline.Text = "  ";
            this.ChangeFontUnderline.Click += new System.EventHandler(this.ChangeFontUnderline_Click);
            // 
            // ChangeFontColor
            // 
            this.ChangeFontColor.BackgroundImage = global::EasyNotes.Properties.Resources.color_palette2;
            this.ChangeFontColor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ChangeFontColor.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ChangeFontColor.Margin = new System.Windows.Forms.Padding(5, 2, 7, 0);
            this.ChangeFontColor.Name = "ChangeFontColor";
            this.ChangeFontColor.Size = new System.Drawing.Size(34, 25);
            this.ChangeFontColor.Text = "     ";
            this.ChangeFontColor.Click += new System.EventHandler(this.ChangeFontColor_Click);
            // 
            // ChangeFontSize
            // 
            this.ChangeFontSize.BackgroundImage = global::EasyNotes.Properties.Resources.font_size;
            this.ChangeFontSize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ChangeFontSize.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ChangeFontSize.Margin = new System.Windows.Forms.Padding(5, 2, 7, 0);
            this.ChangeFontSize.Name = "ChangeFontSize";
            this.ChangeFontSize.Size = new System.Drawing.Size(28, 25);
            this.ChangeFontSize.Text = "   ";
            this.ChangeFontSize.Click += new System.EventHandler(this.ChangeFontSize_Click);
            // 
            // DeleteNoteOption
            // 
            this.DeleteNoteOption.BackgroundImage = global::EasyNotes.Properties.Resources.pad;
            this.DeleteNoteOption.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.DeleteNoteOption.Font = new System.Drawing.Font("Microsoft YaHei UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteNoteOption.Margin = new System.Windows.Forms.Padding(5, 2, 6, 0);
            this.DeleteNoteOption.Name = "DeleteNoteOption";
            this.DeleteNoteOption.Size = new System.Drawing.Size(29, 25);
            this.DeleteNoteOption.Text = "  ";
            this.DeleteNoteOption.Click += new System.EventHandler(this.DeleteNoteOption_Click);
            // 
            // exportToPdfToolStripMenuItem
            // 
            this.exportToPdfToolStripMenuItem.BackgroundImage = global::EasyNotes.Properties.Resources.print;
            this.exportToPdfToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exportToPdfToolStripMenuItem.Name = "exportToPdfToolStripMenuItem";
            this.exportToPdfToolStripMenuItem.Size = new System.Drawing.Size(25, 27);
            this.exportToPdfToolStripMenuItem.Text = "  ";
            this.exportToPdfToolStripMenuItem.Click += new System.EventHandler(this.PrintNote_Click);
            // 
            // NoteEditorTools
            // 
            this.NoteEditorTools.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.NoteEditorTools.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DeleteNoteOption,
            this.ChangeFontBold,
            this.ChangeFontItalic,
            this.ChangeFontUnderline,
            this.ChangeFontSize,
            this.ChangeFontColor,
            this.addImageToolStripMenuItem,
            this.exportToPdfToolStripMenuItem});
            this.NoteEditorTools.Location = new System.Drawing.Point(0, 0);
            this.NoteEditorTools.Name = "NoteEditorTools";
            this.NoteEditorTools.Padding = new System.Windows.Forms.Padding(4, 3, 0, 3);
            this.NoteEditorTools.Size = new System.Drawing.Size(578, 33);
            this.NoteEditorTools.TabIndex = 1;
            this.NoteEditorTools.Text = "menuStrip1";
            // 
            // ChangeFontBold
            // 
            this.ChangeFontBold.BackgroundImage = global::EasyNotes.Properties.Resources.bold;
            this.ChangeFontBold.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ChangeFontBold.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ChangeFontBold.Margin = new System.Windows.Forms.Padding(3, 6, 5, 2);
            this.ChangeFontBold.Name = "ChangeFontBold";
            this.ChangeFontBold.Size = new System.Drawing.Size(22, 19);
            this.ChangeFontBold.Text = " ";
            this.ChangeFontBold.Click += new System.EventHandler(this.ChangeFontBold_Click);
            // 
            // addImageToolStripMenuItem
            // 
            this.addImageToolStripMenuItem.BackgroundImage = global::EasyNotes.Properties.Resources.add_image;
            this.addImageToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.addImageToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addImageToolStripMenuItem.Name = "addImageToolStripMenuItem";
            this.addImageToolStripMenuItem.Size = new System.Drawing.Size(26, 27);
            this.addImageToolStripMenuItem.Text = " ";
            this.addImageToolStripMenuItem.Click += new System.EventHandler(this.addImageToolStripMenuItem_Click_1);
            // 
            // NoteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.NoteTextBox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.NoteEditorTools);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "NoteControl";
            this.Size = new System.Drawing.Size(578, 320);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.NoteEditorTools.ResumeLayout(false);
            this.NoteEditorTools.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PrintPreviewDialog PrintPreviewNote;
        private System.Drawing.Printing.PrintDocument PrintNoteDocument;
        private System.Windows.Forms.RichTextBox NoteTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox Title_Textbox;
        private System.Windows.Forms.ToolStripMenuItem ChangeFontItalic;
        private System.Windows.Forms.ToolStripMenuItem ChangeFontUnderline;
        private System.Windows.Forms.ToolStripMenuItem ChangeFontColor;
        private System.Windows.Forms.ToolStripMenuItem ChangeFontSize;
        private System.Windows.Forms.ToolStripMenuItem DeleteNoteOption;
        private System.Windows.Forms.ToolStripMenuItem exportToPdfToolStripMenuItem;
        private System.Windows.Forms.MenuStrip NoteEditorTools;
        private System.Windows.Forms.ToolStripMenuItem ChangeFontBold;
        private System.Windows.Forms.ToolStripMenuItem addImageToolStripMenuItem;
    }
}
