﻿namespace EasyNotes
{
    partial class MainNoteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FormContainerPanel = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.NotePanel = new System.Windows.Forms.Panel();
            this.NoteListPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.userlbl = new System.Windows.Forms.Label();
            this.CreatNewNotePanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.createNewNote_btn = new System.Windows.Forms.Button();
            this.FormContainerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.CreatNewNotePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // FormContainerPanel
            // 
            this.FormContainerPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.FormContainerPanel.Controls.Add(this.pictureBox4);
            this.FormContainerPanel.Controls.Add(this.pictureBox3);
            this.FormContainerPanel.Controls.Add(this.pictureBox2);
            this.FormContainerPanel.Controls.Add(this.NotePanel);
            this.FormContainerPanel.Controls.Add(this.NoteListPanel);
            this.FormContainerPanel.Controls.Add(this.userlbl);
            this.FormContainerPanel.Controls.Add(this.CreatNewNotePanel);
            this.FormContainerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormContainerPanel.Location = new System.Drawing.Point(0, 0);
            this.FormContainerPanel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FormContainerPanel.Name = "FormContainerPanel";
            this.FormContainerPanel.Size = new System.Drawing.Size(1071, 576);
            this.FormContainerPanel.TabIndex = 0;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.BackgroundImage = global::EasyNotes.Properties.Resources.facebook;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(945, 10);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(27, 29);
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.BackgroundImage = global::EasyNotes.Properties.Resources.instagram2;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(1033, 10);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(27, 29);
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackgroundImage = global::EasyNotes.Properties.Resources.twitter;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(990, 10);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 29);
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // NotePanel
            // 
            this.NotePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NotePanel.Location = new System.Drawing.Point(334, 77);
            this.NotePanel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.NotePanel.Name = "NotePanel";
            this.NotePanel.Size = new System.Drawing.Size(735, 499);
            this.NotePanel.TabIndex = 5;
            // 
            // NoteListPanel
            // 
            this.NoteListPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.NoteListPanel.BackColor = System.Drawing.Color.SteelBlue;
            this.NoteListPanel.Location = new System.Drawing.Point(0, 77);
            this.NoteListPanel.Margin = new System.Windows.Forms.Padding(8, 2, 2, 2);
            this.NoteListPanel.Name = "NoteListPanel";
            this.NoteListPanel.Size = new System.Drawing.Size(330, 497);
            this.NoteListPanel.TabIndex = 4;
            // 
            // userlbl
            // 
            this.userlbl.AutoSize = true;
            this.userlbl.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.userlbl.Font = new System.Drawing.Font("Microsoft YaHei UI", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userlbl.ForeColor = System.Drawing.Color.SteelBlue;
            this.userlbl.Location = new System.Drawing.Point(330, 3);
            this.userlbl.Name = "userlbl";
            this.userlbl.Size = new System.Drawing.Size(212, 46);
            this.userlbl.TabIndex = 3;
            this.userlbl.Text = "Easy Notes";
            // 
            // CreatNewNotePanel
            // 
            this.CreatNewNotePanel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.CreatNewNotePanel.Controls.Add(this.pictureBox1);
            this.CreatNewNotePanel.Controls.Add(this.createNewNote_btn);
            this.CreatNewNotePanel.Location = new System.Drawing.Point(0, 3);
            this.CreatNewNotePanel.Name = "CreatNewNotePanel";
            this.CreatNewNotePanel.Size = new System.Drawing.Size(330, 74);
            this.CreatNewNotePanel.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.SteelBlue;
            this.pictureBox1.BackgroundImage = global::EasyNotes.Properties.Resources.pad__1_;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(252, 21);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // createNewNote_btn
            // 
            this.createNewNote_btn.BackColor = System.Drawing.Color.SteelBlue;
            this.createNewNote_btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.createNewNote_btn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.createNewNote_btn.Font = new System.Drawing.Font("Lucida Sans Unicode", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createNewNote_btn.ForeColor = System.Drawing.Color.White;
            this.createNewNote_btn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.createNewNote_btn.Location = new System.Drawing.Point(34, 16);
            this.createNewNote_btn.Name = "createNewNote_btn";
            this.createNewNote_btn.Size = new System.Drawing.Size(250, 40);
            this.createNewNote_btn.TabIndex = 0;
            this.createNewNote_btn.Text = "Create new Note";
            this.createNewNote_btn.UseVisualStyleBackColor = false;
            this.createNewNote_btn.Click += new System.EventHandler(this.CreateNewNote_btn_Click);
            // 
            // MainNoteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1071, 576);
            this.Controls.Add(this.FormContainerPanel);
            this.Name = "MainNoteForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Easy Notes";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CloseEasyNotes);
            this.Load += new System.EventHandler(this.MainNoteForm_Load);
            this.FormContainerPanel.ResumeLayout(false);
            this.FormContainerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.CreatNewNotePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel FormContainerPanel;
        private System.Windows.Forms.FlowLayoutPanel NoteListPanel;
        private System.Windows.Forms.Label userlbl;
        private System.Windows.Forms.Panel CreatNewNotePanel;
        private System.Windows.Forms.Button createNewNote_btn;
        private System.Windows.Forms.Panel NotePanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}