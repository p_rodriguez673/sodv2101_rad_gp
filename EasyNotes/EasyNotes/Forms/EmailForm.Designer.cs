﻿namespace EasyNotes.Forms
{
    partial class EmailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fromLabel = new System.Windows.Forms.Label();
            this.toLabel = new System.Windows.Forms.Label();
            this.bodyLabel = new System.Windows.Forms.Label();
            this.subjectLabel = new System.Windows.Forms.Label();
            this.senderEmailTextbox = new System.Windows.Forms.TextBox();
            this.recieverEmailTextbox = new System.Windows.Forms.TextBox();
            this.subjectTextbox = new System.Windows.Forms.TextBox();
            this.bodyRichTextbox = new System.Windows.Forms.RichTextBox();
            this.FileName = new System.Windows.Forms.Label();
            this.uploadBtn = new System.Windows.Forms.Button();
            this.sndBtn = new System.Windows.Forms.Button();
            this.fileLabel = new System.Windows.Forms.Label();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // fromLabel
            // 
            this.fromLabel.AutoSize = true;
            this.fromLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.fromLabel.Location = new System.Drawing.Point(28, 23);
            this.fromLabel.Name = "fromLabel";
            this.fromLabel.Size = new System.Drawing.Size(30, 13);
            this.fromLabel.TabIndex = 0;
            this.fromLabel.Text = "From";
            // 
            // toLabel
            // 
            this.toLabel.AutoSize = true;
            this.toLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.toLabel.Location = new System.Drawing.Point(28, 53);
            this.toLabel.Name = "toLabel";
            this.toLabel.Size = new System.Drawing.Size(20, 13);
            this.toLabel.TabIndex = 1;
            this.toLabel.Text = "To";
            // 
            // bodyLabel
            // 
            this.bodyLabel.AutoSize = true;
            this.bodyLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bodyLabel.Location = new System.Drawing.Point(28, 126);
            this.bodyLabel.Name = "bodyLabel";
            this.bodyLabel.Size = new System.Drawing.Size(31, 13);
            this.bodyLabel.TabIndex = 2;
            this.bodyLabel.Text = "Body";
            // 
            // subjectLabel
            // 
            this.subjectLabel.AutoSize = true;
            this.subjectLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.subjectLabel.Location = new System.Drawing.Point(28, 86);
            this.subjectLabel.Name = "subjectLabel";
            this.subjectLabel.Size = new System.Drawing.Size(43, 13);
            this.subjectLabel.TabIndex = 3;
            this.subjectLabel.Text = "Subject";
            // 
            // senderEmailTextbox
            // 
            this.senderEmailTextbox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.senderEmailTextbox.Location = new System.Drawing.Point(86, 20);
            this.senderEmailTextbox.Name = "senderEmailTextbox";
            this.senderEmailTextbox.Size = new System.Drawing.Size(210, 20);
            this.senderEmailTextbox.TabIndex = 4;
            // 
            // recieverEmailTextbox
            // 
            this.recieverEmailTextbox.Location = new System.Drawing.Point(86, 53);
            this.recieverEmailTextbox.Name = "recieverEmailTextbox";
            this.recieverEmailTextbox.Size = new System.Drawing.Size(210, 20);
            this.recieverEmailTextbox.TabIndex = 5;
            // 
            // subjectTextbox
            // 
            this.subjectTextbox.Location = new System.Drawing.Point(86, 86);
            this.subjectTextbox.Name = "subjectTextbox";
            this.subjectTextbox.Size = new System.Drawing.Size(210, 20);
            this.subjectTextbox.TabIndex = 6;
            // 
            // bodyRichTextbox
            // 
            this.bodyRichTextbox.Location = new System.Drawing.Point(86, 126);
            this.bodyRichTextbox.Name = "bodyRichTextbox";
            this.bodyRichTextbox.Size = new System.Drawing.Size(210, 105);
            this.bodyRichTextbox.TabIndex = 7;
            this.bodyRichTextbox.Text = "";
            // 
            // FileName
            // 
            this.FileName.AutoSize = true;
            this.FileName.Location = new System.Drawing.Point(86, 238);
            this.FileName.Name = "FileName";
            this.FileName.Size = new System.Drawing.Size(0, 13);
            this.FileName.TabIndex = 8;
            // 
            // uploadBtn
            // 
            this.uploadBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.uploadBtn.Location = new System.Drawing.Point(89, 264);
            this.uploadBtn.Name = "uploadBtn";
            this.uploadBtn.Size = new System.Drawing.Size(75, 23);
            this.uploadBtn.TabIndex = 9;
            this.uploadBtn.Text = "UploadFile";
            this.uploadBtn.UseVisualStyleBackColor = false;
            this.uploadBtn.Click += new System.EventHandler(this.uploadBtn_Click);
            // 
            // sndBtn
            // 
            this.sndBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.sndBtn.Location = new System.Drawing.Point(199, 264);
            this.sndBtn.Name = "sndBtn";
            this.sndBtn.Size = new System.Drawing.Size(75, 23);
            this.sndBtn.TabIndex = 10;
            this.sndBtn.Text = "Send";
            this.sndBtn.UseVisualStyleBackColor = false;
            this.sndBtn.Click += new System.EventHandler(this.sndBtn_Click);
            // 
            // fileLabel
            // 
            this.fileLabel.AutoSize = true;
            this.fileLabel.Location = new System.Drawing.Point(86, 238);
            this.fileLabel.Name = "fileLabel";
            this.fileLabel.Size = new System.Drawing.Size(0, 13);
            this.fileLabel.TabIndex = 11;
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.AutoSize = true;
            this.fileNameLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.fileNameLabel.Location = new System.Drawing.Point(86, 238);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(48, 13);
            this.fileNameLabel.TabIndex = 12;
            this.fileNameLabel.Text = "fileName";
            // 
            // EmailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(350, 351);
            this.Controls.Add(this.fileNameLabel);
            this.Controls.Add(this.fileLabel);
            this.Controls.Add(this.sndBtn);
            this.Controls.Add(this.uploadBtn);
            this.Controls.Add(this.FileName);
            this.Controls.Add(this.bodyRichTextbox);
            this.Controls.Add(this.subjectTextbox);
            this.Controls.Add(this.recieverEmailTextbox);
            this.Controls.Add(this.senderEmailTextbox);
            this.Controls.Add(this.subjectLabel);
            this.Controls.Add(this.bodyLabel);
            this.Controls.Add(this.toLabel);
            this.Controls.Add(this.fromLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmailForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EmailForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label fromLabel;
        private System.Windows.Forms.Label toLabel;
        private System.Windows.Forms.Label bodyLabel;
        private System.Windows.Forms.Label subjectLabel;
        private System.Windows.Forms.TextBox senderEmailTextbox;
        private System.Windows.Forms.TextBox recieverEmailTextbox;
        private System.Windows.Forms.TextBox subjectTextbox;
        private System.Windows.Forms.RichTextBox bodyRichTextbox;
        private System.Windows.Forms.Label FileName;
        private System.Windows.Forms.Button uploadBtn;
        private System.Windows.Forms.Button sndBtn;
        private System.Windows.Forms.Label fileLabel;
        private System.Windows.Forms.Label fileNameLabel;
    }
}