﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.IO;

namespace EasyNotes.Forms
{
    public partial class EmailForm : Form
    {
        public EmailForm()
        {
            InitializeComponent();
            fileLabel.Visible = false;
            fileNameLabel.Visible = false;
        }

        private void uploadBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog loadFile = new OpenFileDialog();
            loadFile.Filter = "pdf files(*.pdf)| *.pdf | All files(*.*) | *.* ";

            if (loadFile.ShowDialog() == DialogResult.OK)
            {
                var filepath = loadFile.FileName;
                fileNameLabel.Visible = true;
                fileNameLabel.Text = Path.GetFileName(filepath);
                fileLabel.Text = filepath;
            }
        }

        private void sndBtn_Click(object sender, EventArgs e)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress(senderEmailTextbox.Text);
            mail.To.Add(recieverEmailTextbox.Text);
            mail.Subject = subjectTextbox.Text;
            mail.Body = bodyRichTextbox.Text;

            Attachment attachment;
            attachment = new Attachment(fileLabel.Text);
            mail.Attachments.Add(attachment);

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new NetworkCredential(senderEmailTextbox.Text, "Password");
            SmtpServer.EnableSsl = true;
  
            SmtpServer.Send(mail);
            MessageBox.Show("Email has been sent successfully");
            this.Close();
            
        }
    }
}
