﻿using EasyNotesLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyNotes
{
    public partial class LoginForm : Form
    {

        public LoginForm()
        {
            InitializeComponent();

            // Hashing password with an asterisk
            passwordTxt.PasswordChar = '*';
            passwordTxt.CharacterCasing = CharacterCasing.Lower;

            // setting icons visibility to false
            verifyEmail.Visible = false;
            verifyPassword.Visible = false;

            errorEmail.Visible = false;
            errorPassword.Visible = false;

            userNameTxt.CausesValidation = true;
            passwordTxt.CausesValidation = true;

            userNameTxt.Validating += new System.ComponentModel.CancelEventHandler(UserNameTxt_Validating);
            passwordTxt.Validating += new System.ComponentModel.CancelEventHandler(PasswordTxt_Validating);
        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            AuthenticateUser();
        }

        private async void AuthenticateUser()
        {
            // Getting user name and password to validate login
            string userEmail = userNameTxt.Text;
            string userPassword = passwordTxt.Text;
            Loader loader = new Loader();
            this.Hide();
            loader.Show();
            User loggedInUser = null;
            await Task.Run(()=> 
            {
                try
                {

                    loggedInUser = new User(userEmail, userPassword);

                    
                }
                catch (Exception)
                {
                   
                }
            });
            // If user exists in database, go to main form
            if (loggedInUser != null)
            {
                MainNoteForm noteAppForm = new MainNoteForm(loggedInUser);
                noteAppForm.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Invalid ID & password! Please try again later.");
            }
            loader.Dispose();
        }

        private void UserNameTxt_Validating(object sender, CancelEventArgs e)
        {
            if (userNameTxt.Text.Length == 0)
            {
                errorEmail.Visible = true;
            }
            else if (userNameTxt.Text.Length > 0)
            {
                verifyEmail.Visible = true;
                errorEmail.Visible = false;
            }
        }

        private void PasswordTxt_Validating(object sender, CancelEventArgs e)
        {
            if (passwordTxt.Text.Length == 0)
            {
                errorPassword.Visible = true;
            }

            else if (passwordTxt.Text.Length > 0)
            {
                verifyPassword.Visible = true;
                errorPassword.Visible = false;
            }
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (Application.OpenForms.Count < 1)
            {
                Application.Exit();
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            string email = emailRegistertxt.Text;
            string userName = userRegistertxt.Text;
            string passwordRegistration = passRegistertxt.Text;

            User newUser = new User();

            if (passwordRegistration != confirmPasstxt.Text)
            {
                MessageBox.Show("Password does not match!", "EasyNotes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            else
            {
                RegisterAndAuthenticate(newUser, userName, email, passwordRegistration);
            }           
        }

        private async void RegisterAndAuthenticate(User newUser, string userName, string email, string password)
        {
            Loader loader = new Loader();
            this.Hide();
            loader.Show();
            User loggedInUser = null;
            await Task.Run(() =>
            {
                // If user exists, proceed to main form
                newUser.InsertNewUser(userName, email, password);
                MessageBox.Show("User registered succesfully", "EasyNotes", MessageBoxButtons.OK, MessageBoxIcon.Information);

                loggedInUser = new User(email, password);
            });
            // If user exists in database, go to main form
            if (loggedInUser != null)
            {
                MainNoteForm noteAppForm = new MainNoteForm(loggedInUser);
                noteAppForm.Show();
                this.Close();
            }
            loader.Dispose();
        }
    }
}
