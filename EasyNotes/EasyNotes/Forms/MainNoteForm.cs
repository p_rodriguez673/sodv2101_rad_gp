﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using EasyNotesLib;

namespace EasyNotes
{
    public partial class MainNoteForm : Form
    {
        private User User;

        public MainNoteForm(User user)
        {
            this.User = user;
            InitializeComponent();
        }

        //calling save note object from user class
        private void SaveNote(Note note)
        {
            this.User.SaveNote(note);
        }


        private void MainNoteForm_Load(object sender, EventArgs e)
        {
            // when form loads load first list all the notes
            LoadNoteList();
        }

        // create a notelist in main form with the list of notes available from user object
        private void LoadNoteList()
        {
            this.NoteListPanel.Controls.Clear();
            foreach (var note in User.Notes)
            {
                var NoteListItem = new NoteListItemControl(GetNewNoteControl(note, true));
                NoteListItem.Title = note.Title;
                NoteListItem.ShowNote_Event += LoadNoteToMainForm();
                this.NoteListPanel.Controls.Add(NoteListItem);
            }
        }

        //create a new notecontrol
        private void CreateNewNote_btn_Click(object sender, EventArgs e)
        {
            var noteListItemControl = new NoteListItemControl(GetNewNoteControl(new Note("untitled")));
            noteListItemControl.ShowNote_Event += LoadNoteToMainForm();
            this.NoteListPanel.Controls.Add(noteListItemControl);
            noteListItemControl.OpenNote();
        }

        // gets a new note control with anchoring and size of the notepanl
        private NoteControl GetNewNoteControl(Note note, bool savedFile = false)
        {
            NoteControl noteControl = new NoteControl(note, SaveNote, RemoveNoteFromMainFormAndDB(), savedFile)
            {
                Anchor = (AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom),
                Size = new Size(NotePanel.Size.Width, NotePanel.Size.Height)
            };
            return noteControl;
        }

        //events fires when user selects note from list of note
        //action will be executed in NoteListItemControl ShowNoteEvent
        private Action<NoteControl> LoadNoteToMainForm()
        {
            return notecontrol =>
            {
                notecontrol.Size = this.NotePanel.Size;
                foreach (Control control in NotePanel.Controls)
                {
                    if (control.GetType() == notecontrol.GetType())
                    {
                        ((NoteControl) control).SaveNoteToDatabaseAsync();
                    }
                    this.NotePanel.Controls.Remove(control);
                }
                this.NotePanel.Controls.Add(notecontrol);
                notecontrol.LoadNote();
            };
        }

        public async void DeleteNoteDBAsync(Note note)
        {
            Loader loader = new Loader();
            loader.Show();
            await Task.Run(() => User.DeleteNote(note));
            loader.Dispose();
        }

        //action will be called when user deletes note 
        // action will be called in NoteControl Class when delete event fires
        private Action<Note, NoteControl> RemoveNoteFromMainFormAndDB()
        {
            return (note, noteControl) =>
            {
                this.NotePanel.Controls.Remove(noteControl);
                DeleteNoteDBAsync(note);
                //converting all the control from noteListPanel to List<noteListItem> to run the query and remove control smoothly from UI
                var list = new List<NoteListItemControl>();
                foreach (NoteListItemControl item in NoteListPanel.Controls)
                {
                    list.Add(item);
                }
                var noteToRemoveList = list.Where(notecontrolitem => notecontrolitem.NoteControl.Note == note).ToList();
                if (noteToRemoveList.Count > 0 && noteToRemoveList[0] != null)
                {
                    NoteListPanel.Controls.Remove(noteToRemoveList[0]);
                }
                else
                {
                    //reload  list in case query gives null
                    LoadNoteList();
                }
            };
        }

        //function to save all the pending savings and exit application
        private void SaveAndExit()
        {
            foreach (NoteListItemControl noteListControl in this.NoteListPanel.Controls)
            {
                noteListControl.NoteControl.SaveNoteToDatabaseAsync();
            }
            //sleep main thread until it finishes unfinished savings if there any left.
            Loader loader = new Loader();
            loader.Show();
            Thread.Sleep(3000);
            loader.Dispose();
            if (Application.OpenForms.Count == 1)
            {
                Environment.Exit(1);
            }
        }

        private void CloseEasyNotes(object sender, FormClosingEventArgs e)
        {
            // stop default exit application
            // save all pending saving to database and exit
            e.Cancel = true;
            this.Hide();
            SaveAndExit();
        }

    }
}
