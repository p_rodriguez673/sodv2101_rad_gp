﻿namespace EasyNotes
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelBrand = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ImageLogo = new System.Windows.Forms.PictureBox();
            this.panelLogin = new System.Windows.Forms.GroupBox();
            this.LoginInputsPanel = new System.Windows.Forms.Panel();
            this.lblLoginUser = new System.Windows.Forms.Label();
            this.errorPassword = new System.Windows.Forms.PictureBox();
            this.userNameTxt = new System.Windows.Forms.TextBox();
            this.errorEmail = new System.Windows.Forms.PictureBox();
            this.passwordTxt = new System.Windows.Forms.TextBox();
            this.verifyPassword = new System.Windows.Forms.PictureBox();
            this.lblLoginPassword = new System.Windows.Forms.Label();
            this.verifyEmail = new System.Windows.Forms.PictureBox();
            this.errorMessage = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.panelRegister = new System.Windows.Forms.GroupBox();
            this.btnRegister = new System.Windows.Forms.Button();
            this.lblRegisterRePassword = new System.Windows.Forms.Label();
            this.lblRegisterPassword = new System.Windows.Forms.Label();
            this.lblRegisterFullname = new System.Windows.Forms.Label();
            this.lblRegisterName = new System.Windows.Forms.Label();
            this.confirmPasstxt = new System.Windows.Forms.TextBox();
            this.emailRegistertxt = new System.Windows.Forms.TextBox();
            this.passRegistertxt = new System.Windows.Forms.TextBox();
            this.userRegistertxt = new System.Windows.Forms.TextBox();
            this.PanelBrand.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageLogo)).BeginInit();
            this.panelLogin.SuspendLayout();
            this.LoginInputsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.verifyPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.verifyEmail)).BeginInit();
            this.panelRegister.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelBrand
            // 
            this.PanelBrand.BackColor = System.Drawing.Color.SteelBlue;
            this.PanelBrand.Controls.Add(this.panel1);
            this.PanelBrand.Controls.Add(this.panelLogin);
            this.PanelBrand.Controls.Add(this.panelRegister);
            this.PanelBrand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelBrand.Location = new System.Drawing.Point(0, 0);
            this.PanelBrand.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PanelBrand.Name = "PanelBrand";
            this.PanelBrand.Size = new System.Drawing.Size(1325, 671);
            this.PanelBrand.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.ImageLogo);
            this.panel1.Location = new System.Drawing.Point(505, 106);
            this.panel1.Margin = new System.Windows.Forms.Padding(1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(271, 364);
            this.panel1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(53, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 85);
            this.label2.TabIndex = 4;
            this.label2.Text = "Notes";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(67, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 85);
            this.label1.TabIndex = 3;
            this.label1.Text = "Easy";
            // 
            // ImageLogo
            // 
            this.ImageLogo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ImageLogo.BackgroundImage = global::EasyNotes.Properties.Resources.notes;
            this.ImageLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ImageLogo.Location = new System.Drawing.Point(52, 201);
            this.ImageLogo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ImageLogo.Name = "ImageLogo";
            this.ImageLogo.Size = new System.Drawing.Size(168, 164);
            this.ImageLogo.TabIndex = 2;
            this.ImageLogo.TabStop = false;
            // 
            // panelLogin
            // 
            this.panelLogin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelLogin.BackColor = System.Drawing.Color.White;
            this.panelLogin.Controls.Add(this.LoginInputsPanel);
            this.panelLogin.Controls.Add(this.errorMessage);
            this.panelLogin.Controls.Add(this.btnLogin);
            this.panelLogin.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelLogin.ForeColor = System.Drawing.Color.SteelBlue;
            this.panelLogin.Location = new System.Drawing.Point(12, 12);
            this.panelLogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelLogin.Name = "panelLogin";
            this.panelLogin.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelLogin.Size = new System.Drawing.Size(489, 646);
            this.panelLogin.TabIndex = 0;
            this.panelLogin.TabStop = false;
            this.panelLogin.Text = "Do you have an account? Login";
            // 
            // LoginInputsPanel
            // 
            this.LoginInputsPanel.Controls.Add(this.lblLoginUser);
            this.LoginInputsPanel.Controls.Add(this.errorPassword);
            this.LoginInputsPanel.Controls.Add(this.userNameTxt);
            this.LoginInputsPanel.Controls.Add(this.errorEmail);
            this.LoginInputsPanel.Controls.Add(this.passwordTxt);
            this.LoginInputsPanel.Controls.Add(this.verifyPassword);
            this.LoginInputsPanel.Controls.Add(this.lblLoginPassword);
            this.LoginInputsPanel.Controls.Add(this.verifyEmail);
            this.LoginInputsPanel.Location = new System.Drawing.Point(5, 43);
            this.LoginInputsPanel.Margin = new System.Windows.Forms.Padding(1);
            this.LoginInputsPanel.Name = "LoginInputsPanel";
            this.LoginInputsPanel.Size = new System.Drawing.Size(480, 250);
            this.LoginInputsPanel.TabIndex = 9;
            // 
            // lblLoginUser
            // 
            this.lblLoginUser.AutoSize = true;
            this.lblLoginUser.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoginUser.Location = new System.Drawing.Point(3, 14);
            this.lblLoginUser.Name = "lblLoginUser";
            this.lblLoginUser.Size = new System.Drawing.Size(65, 26);
            this.lblLoginUser.TabIndex = 3;
            this.lblLoginUser.Text = "Email";
            // 
            // errorPassword
            // 
            this.errorPassword.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.errorPassword.BackgroundImage = global::EasyNotes.Properties.Resources.exclamation_mark;
            this.errorPassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.errorPassword.Location = new System.Drawing.Point(431, 145);
            this.errorPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.errorPassword.Name = "errorPassword";
            this.errorPassword.Size = new System.Drawing.Size(49, 37);
            this.errorPassword.TabIndex = 8;
            this.errorPassword.TabStop = false;
            // 
            // userNameTxt
            // 
            this.userNameTxt.Location = new System.Drawing.Point(3, 43);
            this.userNameTxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.userNameTxt.Name = "userNameTxt";
            this.userNameTxt.Size = new System.Drawing.Size(423, 37);
            this.userNameTxt.TabIndex = 2;
            // 
            // errorEmail
            // 
            this.errorEmail.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.errorEmail.BackgroundImage = global::EasyNotes.Properties.Resources.exclamation_mark;
            this.errorEmail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.errorEmail.Location = new System.Drawing.Point(432, 43);
            this.errorEmail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.errorEmail.Name = "errorEmail";
            this.errorEmail.Size = new System.Drawing.Size(49, 37);
            this.errorEmail.TabIndex = 8;
            this.errorEmail.TabStop = false;
            // 
            // passwordTxt
            // 
            this.passwordTxt.Location = new System.Drawing.Point(3, 148);
            this.passwordTxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.passwordTxt.Name = "passwordTxt";
            this.passwordTxt.Size = new System.Drawing.Size(423, 37);
            this.passwordTxt.TabIndex = 2;
            // 
            // verifyPassword
            // 
            this.verifyPassword.BackgroundImage = global::EasyNotes.Properties.Resources.checkmark;
            this.verifyPassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.verifyPassword.Location = new System.Drawing.Point(431, 145);
            this.verifyPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.verifyPassword.Name = "verifyPassword";
            this.verifyPassword.Size = new System.Drawing.Size(49, 37);
            this.verifyPassword.TabIndex = 8;
            this.verifyPassword.TabStop = false;
            // 
            // lblLoginPassword
            // 
            this.lblLoginPassword.AutoSize = true;
            this.lblLoginPassword.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoginPassword.Location = new System.Drawing.Point(3, 116);
            this.lblLoginPassword.Name = "lblLoginPassword";
            this.lblLoginPassword.Size = new System.Drawing.Size(102, 26);
            this.lblLoginPassword.TabIndex = 4;
            this.lblLoginPassword.Text = "Password";
            // 
            // verifyEmail
            // 
            this.verifyEmail.BackgroundImage = global::EasyNotes.Properties.Resources.checkmark;
            this.verifyEmail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.verifyEmail.Location = new System.Drawing.Point(431, 43);
            this.verifyEmail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.verifyEmail.Name = "verifyEmail";
            this.verifyEmail.Size = new System.Drawing.Size(49, 37);
            this.verifyEmail.TabIndex = 8;
            this.verifyEmail.TabStop = false;
            // 
            // errorMessage
            // 
            this.errorMessage.AutoSize = true;
            this.errorMessage.ForeColor = System.Drawing.Color.Red;
            this.errorMessage.Location = new System.Drawing.Point(16, 295);
            this.errorMessage.Name = "errorMessage";
            this.errorMessage.Size = new System.Drawing.Size(0, 31);
            this.errorMessage.TabIndex = 7;
            // 
            // btnLogin
            // 
            this.btnLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLogin.BackColor = System.Drawing.Color.SteelBlue;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.ForeColor = System.Drawing.Color.White;
            this.btnLogin.Location = new System.Drawing.Point(171, 555);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(156, 58);
            this.btnLogin.TabIndex = 5;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.LoginBtn_Click);
            // 
            // panelRegister
            // 
            this.panelRegister.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelRegister.BackColor = System.Drawing.Color.White;
            this.panelRegister.Controls.Add(this.btnRegister);
            this.panelRegister.Controls.Add(this.lblRegisterRePassword);
            this.panelRegister.Controls.Add(this.lblRegisterPassword);
            this.panelRegister.Controls.Add(this.lblRegisterFullname);
            this.panelRegister.Controls.Add(this.lblRegisterName);
            this.panelRegister.Controls.Add(this.confirmPasstxt);
            this.panelRegister.Controls.Add(this.emailRegistertxt);
            this.panelRegister.Controls.Add(this.passRegistertxt);
            this.panelRegister.Controls.Add(this.userRegistertxt);
            this.panelRegister.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelRegister.ForeColor = System.Drawing.Color.SteelBlue;
            this.panelRegister.Location = new System.Drawing.Point(775, 12);
            this.panelRegister.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelRegister.Name = "panelRegister";
            this.panelRegister.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelRegister.Size = new System.Drawing.Size(539, 646);
            this.panelRegister.TabIndex = 1;
            this.panelRegister.TabStop = false;
            this.panelRegister.Text = "Sign Up!";
            // 
            // btnRegister
            // 
            this.btnRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRegister.BackColor = System.Drawing.Color.SteelBlue;
            this.btnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegister.ForeColor = System.Drawing.Color.White;
            this.btnRegister.Location = new System.Drawing.Point(212, 555);
            this.btnRegister.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(156, 58);
            this.btnRegister.TabIndex = 7;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = false;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // lblRegisterRePassword
            // 
            this.lblRegisterRePassword.AutoSize = true;
            this.lblRegisterRePassword.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegisterRePassword.Location = new System.Drawing.Point(5, 380);
            this.lblRegisterRePassword.Name = "lblRegisterRePassword";
            this.lblRegisterRePassword.Size = new System.Drawing.Size(240, 26);
            this.lblRegisterRePassword.TabIndex = 6;
            this.lblRegisterRePassword.Text = "Re-enter your password";
            // 
            // lblRegisterPassword
            // 
            this.lblRegisterPassword.AutoSize = true;
            this.lblRegisterPassword.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegisterPassword.Location = new System.Drawing.Point(5, 267);
            this.lblRegisterPassword.Name = "lblRegisterPassword";
            this.lblRegisterPassword.Size = new System.Drawing.Size(102, 26);
            this.lblRegisterPassword.TabIndex = 5;
            this.lblRegisterPassword.Text = "Password";
            // 
            // lblRegisterFullname
            // 
            this.lblRegisterFullname.AutoSize = true;
            this.lblRegisterFullname.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegisterFullname.Location = new System.Drawing.Point(5, 172);
            this.lblRegisterFullname.Name = "lblRegisterFullname";
            this.lblRegisterFullname.Size = new System.Drawing.Size(114, 26);
            this.lblRegisterFullname.TabIndex = 4;
            this.lblRegisterFullname.Text = "User name";
            // 
            // lblRegisterName
            // 
            this.lblRegisterName.AutoSize = true;
            this.lblRegisterName.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegisterName.Location = new System.Drawing.Point(5, 76);
            this.lblRegisterName.Name = "lblRegisterName";
            this.lblRegisterName.Size = new System.Drawing.Size(65, 26);
            this.lblRegisterName.TabIndex = 3;
            this.lblRegisterName.Text = "Email";
            // 
            // confirmPasstxt
            // 
            this.confirmPasstxt.Location = new System.Drawing.Point(5, 410);
            this.confirmPasstxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.confirmPasstxt.Name = "confirmPasstxt";
            this.confirmPasstxt.Size = new System.Drawing.Size(527, 37);
            this.confirmPasstxt.TabIndex = 2;
            // 
            // emailRegistertxt
            // 
            this.emailRegistertxt.Location = new System.Drawing.Point(5, 106);
            this.emailRegistertxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.emailRegistertxt.Name = "emailRegistertxt";
            this.emailRegistertxt.Size = new System.Drawing.Size(527, 37);
            this.emailRegistertxt.TabIndex = 2;
            // 
            // passRegistertxt
            // 
            this.passRegistertxt.Location = new System.Drawing.Point(5, 297);
            this.passRegistertxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.passRegistertxt.Name = "passRegistertxt";
            this.passRegistertxt.Size = new System.Drawing.Size(527, 37);
            this.passRegistertxt.TabIndex = 2;
            // 
            // userRegistertxt
            // 
            this.userRegistertxt.Location = new System.Drawing.Point(5, 201);
            this.userRegistertxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.userRegistertxt.Name = "userRegistertxt";
            this.userRegistertxt.Size = new System.Drawing.Size(527, 37);
            this.userRegistertxt.TabIndex = 2;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1325, 671);
            this.Controls.Add(this.PanelBrand);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "                                                                                 " +
    "                               :::-Easy Notes-:::";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LoginForm_FormClosed);
            this.PanelBrand.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageLogo)).EndInit();
            this.panelLogin.ResumeLayout(false);
            this.panelLogin.PerformLayout();
            this.LoginInputsPanel.ResumeLayout(false);
            this.LoginInputsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.verifyPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.verifyEmail)).EndInit();
            this.panelRegister.ResumeLayout(false);
            this.panelRegister.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelBrand;
        private System.Windows.Forms.GroupBox panelRegister;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.Label lblRegisterRePassword;
        private System.Windows.Forms.Label lblRegisterPassword;
        private System.Windows.Forms.Label lblRegisterFullname;
        private System.Windows.Forms.Label lblRegisterName;
        private System.Windows.Forms.TextBox confirmPasstxt;
        private System.Windows.Forms.TextBox emailRegistertxt;
        private System.Windows.Forms.TextBox passRegistertxt;
        private System.Windows.Forms.TextBox userRegistertxt;
        private System.Windows.Forms.GroupBox panelLogin;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lblLoginPassword;
        private System.Windows.Forms.Label lblLoginUser;
        private System.Windows.Forms.TextBox passwordTxt;
        private System.Windows.Forms.TextBox userNameTxt;
        private System.Windows.Forms.Label errorMessage;
        private System.Windows.Forms.PictureBox ImageLogo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox errorPassword;
        private System.Windows.Forms.PictureBox errorEmail;
        private System.Windows.Forms.PictureBox verifyPassword;
        private System.Windows.Forms.PictureBox verifyEmail;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel LoginInputsPanel;
    }
}

