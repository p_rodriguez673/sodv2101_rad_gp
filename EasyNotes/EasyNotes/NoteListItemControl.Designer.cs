﻿namespace EasyNotes
{
    partial class NoteListItemControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NoteOpenBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // NoteOpenBtn
            // 
            this.NoteOpenBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NoteOpenBtn.BackColor = System.Drawing.Color.DarkOrange;
            this.NoteOpenBtn.Font = new System.Drawing.Font("Microsoft YaHei", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoteOpenBtn.ForeColor = System.Drawing.Color.White;
            this.NoteOpenBtn.Location = new System.Drawing.Point(0, 0);
            this.NoteOpenBtn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.NoteOpenBtn.Name = "NoteOpenBtn";
            this.NoteOpenBtn.Size = new System.Drawing.Size(430, 47);
            this.NoteOpenBtn.TabIndex = 1;
            this.NoteOpenBtn.UseVisualStyleBackColor = false;
            this.NoteOpenBtn.Click += new System.EventHandler(this.NoteOpenBtn_Click);
            // 
            // NoteListItemControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.NoteOpenBtn);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "NoteListItemControl";
            this.Size = new System.Drawing.Size(430, 49);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button NoteOpenBtn;
    }
}
