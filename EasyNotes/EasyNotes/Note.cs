﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyNotes
{
    public class Note
    {
        public string Title { get; set; }
        public int Id { get; set; }
        public List<int> Sequence { get; set; }
        public List<IContent> Content { get; set; }
    }
}
