﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EasyNotesLib;
using EasyNotes.Forms;

namespace EasyNotes
{
    public partial class NoteControl : UserControl
    {
        public Note Note { get; set; }
        private bool SavedFile;
        OpenFileDialog loadFile = new OpenFileDialog();

        //event to save note async
        public event Action<Note> SaveNote_Event;
        //event to delete note async
        public event Action<Note, NoteControl> DeleteNote_Event;

        //
        public NoteControl(Note note, Action<Note> saveNote, Action<Note, NoteControl> deleteNote, bool savedFile = false)
        {
            this.Note = note;
            this.SavedFile = savedFile;
            this.SaveNote_Event += saveNote;
            this.DeleteNote_Event += deleteNote;
            InitializeComponent();
            this.SaveNoteToDatabaseAsync();
            Title_Textbox.DataBindings.Add(new Binding("Text", this.Note, "Title", true, DataSourceUpdateMode.OnPropertyChanged));
            this.Title_Textbox.Text = "untitled";
        }

        //load note to the notecontrol's richtextbox from note object
        public void LoadNote()
        {
            this.NoteTextBox.Rtf = Note.MarkUp;
        }

        private void ChangeFontBold_Click(object sender, EventArgs e)
        {
            if (NoteTextBox.SelectionFont.Bold == false)
            {
                NoteTextBox.SelectionFont = new Font(NoteTextBox.Font, FontStyle.Bold);
            }
            else
            {
                NoteTextBox.SelectionFont = new Font(NoteTextBox.Font, FontStyle.Regular);
            }
        }

        private void ChangeFontItalic_Click(object sender, EventArgs e)
        {
            if (NoteTextBox.SelectionFont.Italic == false)
            {
                NoteTextBox.SelectionFont = new Font(NoteTextBox.Font, FontStyle.Italic);
            }
            else
            {
                NoteTextBox.SelectionFont = new Font(NoteTextBox.Font, FontStyle.Regular);
            }
        }

        private void ChangeFontUnderline_Click(object sender, EventArgs e)
        {
            if (NoteTextBox.SelectionFont.Underline == false)
            {
                NoteTextBox.SelectionFont = new Font(NoteTextBox.Font, FontStyle.Underline);
            }
            else
            {
                NoteTextBox.SelectionFont = new Font(NoteTextBox.Font, FontStyle.Regular);
            }
        }

        private void ChangeFontColor_Click(object sender, EventArgs e)
        {
            ColorDialog selectColor = new ColorDialog();

            selectColor.Color = NoteTextBox.SelectionColor;

            if (selectColor.ShowDialog() == DialogResult.OK)
            {
                NoteTextBox.SelectionColor = selectColor.Color;
            }
        }

        private void ChangeFontSize_Click(object sender, EventArgs e)
        {
            FontDialog selectSize = new FontDialog();

            selectSize.Font = NoteTextBox.SelectionFont;

            if (selectSize.ShowDialog() == DialogResult.OK)
            {
                NoteTextBox.SelectionFont = selectSize.Font;
            }
        }

        private void PrintPreview_Click(object sender, EventArgs e)
        {

            PrintPreviewNote.Document = PrintNoteDocument;
            PrintPreviewNote.ShowDialog();
        }

        private void PrintNote_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog()
                {
                    Title = "Save and share word file",
                    DefaultExt = ".rtf"
                };
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    System.IO.File.WriteAllText(saveFileDialog.FileName, NoteTextBox.Rtf);

                }
            }
            catch (Exception)
            {
                MessageBox.Show("Sorry couldn't print! Please try again.");
            }
        }

        // save note to database Async
        // saveNoe() function will be called by event from mainForm
        // function will be called when user switches between tabs and exit application async
        public async void SaveNoteToDatabaseAsync()
        {
            if (this.SavedFile == false)
            {
                Note.Title = Title_Textbox.Text;
                this.Note.MarkUp = NoteTextBox.Rtf;
                
                    await Task.Run(() =>
                    {
                        try
                        {
                            this.SaveNote_Event(this.Note);
                            this.SavedFile = true;
                        }
                        catch (Exception) { }
            });
                
                
            }
        }

        // DeleteNoeteEvent fires when user click on delete button
        // event removes notecontrol and noteListItemControl from list of notes available
        // event calls RemoveNoteFromMainFormAndDB() to remove notecontrol from list
        private void DeleteNoteOption_Click(object sender, EventArgs e)
        {
            this.DeleteNote_Event(this.Note, this);
        }

        //when text change savedFile prop becomes falls so SaveNoteToDatabaseAsync can update note to db
        private void NoteTextBox_TextChanged(object sender, EventArgs e)
        {
            this.SavedFile = false;
        }


        private void shareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmailForm emailForm = new EmailForm();
            emailForm.Show();
        }

        //when title changes change the value change prop savedfile to false
        // so SaveNoteToDatabaseAsync can update note to db async
        private void Title_TextChanged(object sender, EventArgs e)
        {
            this.SavedFile = false;
        }
        
        static Image ReSizeUserImage(Image image)
        {
            // Current image and height values
            int imageWidth = image.Width;
            int imageHeight = image.Height;

            int fixedWidth = 450;
            int fixedHeight = 300;

            // New points to frame the image
            int x = 0;
            int y = 0;

            // New width and height values
            int newWidthImage = 0;
            int newHeightImage = 0;

            int sizeConvertedWidth = fixedWidth;
            int sizeConvertedHeight = fixedHeight;

            // New image with re-sized measures
            Bitmap bitImage = new Bitmap(image, new Size(sizeConvertedWidth, sizeConvertedHeight));

            Graphics imageGraphics = Graphics.FromImage(bitImage);
            imageGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            imageGraphics.DrawImage(image, new Rectangle(newWidthImage, newHeightImage, sizeConvertedWidth, sizeConvertedHeight), new Rectangle(x, y, imageWidth, imageHeight), GraphicsUnit.Pixel);

            return bitImage;
        }

        private void addImageToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.SavedFile = false;
            loadFile.Title = "Select Image";
            loadFile.Filter = "JPEGs|*.jpg|GIFs|*.gif|Bitmaps|*.bmp|AllFiles|*.*";

            if (loadFile.ShowDialog() == DialogResult.OK)
            {
                var filepath = loadFile.FileName;
                Image image = Image.FromFile(filepath);

                // Re-sizing image        
                image = ReSizeUserImage(image);

                Clipboard.Clear();
                Clipboard.SetImage(image);

                NoteTextBox.Paste();
                NoteTextBox.Focus();
            }

            SaveWithLoader();
        }

        private async void SaveWithLoader()
        {
            Note.Title = Title_Textbox.Text;
            this.Note.MarkUp = NoteTextBox.Rtf;
            Loader loader = new Loader();
            loader.Show();
            //disable main form UI
            this.Parent.Parent.Parent.Hide();
            await Task.Run(() =>
            {
                try
                {
                    this.SaveNote_Event(this.Note);
                    this.SavedFile = true;
                }
                catch (Exception)
                {
                    MessageBox.Show("Something went wrong! Make sure your account is logged at only one device or internet might be slow.");
                }
            });
            //disable main form UI
            this.Parent.Parent.Parent.Show();
            loader.Dispose();
        }
    }
}
