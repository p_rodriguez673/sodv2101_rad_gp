﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyNotes
{
    public partial class NoteListItemControl : UserControl
    {
        //refrence to Custom control for note
        public NoteControl NoteControl;

        //opens notecontrol when user clicks on this
        public event Action<NoteControl> ShowNote_Event;

        public string Title
        {
            get { return NoteOpenBtn.Text; }
            set { NoteOpenBtn.Text = value; }
        }

        public NoteListItemControl(NoteControl noteControl)
        {
            InitializeComponent();
            NoteControl = noteControl;
            NoteOpenBtn.Text = NoteControl.Note.Title;
            NoteOpenBtn.DataBindings.Add(new Binding("Text", NoteControl.Note, "Title", true, DataSourceUpdateMode.OnPropertyChanged));
        }

        //fire a event shownote when user clicks on the this control
        // LoadNoteToMainForm() action will be execute from main form to add notecontrol in main form
        private void NoteOpenBtn_Click(object sender, EventArgs e)
        {
            ShowNote_Event(this.NoteControl);
        }

        //function to fire a event outside the class manually
        public void OpenNote()
        {
            ShowNote_Event(this.NoteControl);
        }
    }
}
